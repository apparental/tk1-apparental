from django import forms

class CreateCartForm(forms.Form):
    Cart_name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Your cart name',
        'type' : 'text',
        'required' : True
    }))