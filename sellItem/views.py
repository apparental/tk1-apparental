from django.shortcuts import render,redirect
from .form import addItem
from .models import Category

# Create your views here.
def saveItem(request):
	if request.method == 'POST':
		form = addItem(request.POST)
		if form.is_valid():
			Category.objects.create(**form.cleaned_data)
		form = addItem()
		categorys = Category.objects.all()
		context ={
			'form' : form,
			'category' : categorys
		}
		return render(request, 'sellItem/sellitem.html', context)
	else:
		form = addItem()
		categorys = Category.objects.all()
		context ={
			'form' : form,
			'category' : categorys
		}
	return render(request, 'sellItem/sellitem.html', context )