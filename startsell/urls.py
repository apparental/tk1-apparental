from django.urls import path
from . import views

app_name = 'startsell'

urlpatterns= [
	path('', views.startsell, name = 'startsell')
]