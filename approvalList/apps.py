from django.apps import AppConfig


class ApprovallistConfig(AppConfig):
    name = 'approvalList'
