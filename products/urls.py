from django.urls import path
from . import views

appname = 'products'

urlpatterns = [
    path('', views.products, name='products')
]