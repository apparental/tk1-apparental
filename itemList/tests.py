from django.test import TestCase, Client
from django.test import Client
from django.test import SimpleTestCase
from django.urls import reverse, resolve
from itemList.views import itemcart
from .forms import CreateCartForm
from .models import Cart
# from itemList.models import cart_name, name, price, status


# Create your tests here.

class TestUrls(SimpleTestCase):
    def test_itemlist_url_isresolved(self):
        url = reverse('itemcart')
        print(resolve(url))
        self.assertEquals(resolve(url).func, itemcart)

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_button_ReturnItem_exist(self):
        response = self.client.get('/itemlist/')
        content = response.content.decode('utf8')
        self.assertIn("<Button", content)
        self.assertIn("Return Item", content)

    # def test_field_CartItem_exist(self):
    #     response = self.client.get('/itemlist/')
    #     content = response.content.decode('utf8')
    #     self.assertIn()
    
    def test_forms(self):
        form = CreateCartForm(data={'cart_name'})
        self.assertTrue(form.is_valid)

    def test_views(self):
        found = resolve('/itemcart/')
        self.assertEqual(found.func, itemlist)
    
    def test_view_uses_correct_template(self):
        c = Client()
        response = c.get('/item-list/')
        self.assertTemplateUsed(response, 'itemlist.html')
    
    def test_home_page_contains_correct_html(self):
        c = Client()
        response = c.get('/item-list/')
        self.assertContains(response, '<h5>Items you Rented</h5>')
