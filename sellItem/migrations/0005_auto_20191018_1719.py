# Generated by Django 2.0.7 on 2019-10-18 17:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sellItem', '0004_auto_20191018_1712'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='category',
            field=models.CharField(choices=[('All', 'All'), ('Dress', 'Dress'), ('T-Shirt', 'T-Shirt'), ('Skirt', 'Skirt'), ('Jumpsuit', 'Jumpsuit'), ('Pants & Leggings', 'Pants & Leggings'), ('Jeans', 'Jeans'), ('Swimwear', 'Swimwear'), ('Knitwear & Cardigans', 'Knitwear & Cardigans'), ('Hoodies & Sweatshirts', 'Hoodies & Sweatshirts'), ('Blazer', 'Blazer'), ('Jacket & Coats', 'Jacket & Coats'), ('Sleepwear', 'Sleepwear'), ('Accessories', 'Accessories')], default='All', max_length=100),
        ),
    ]
