from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import detailBarang
from sellItem.models import Category
from .models import *
import datetime

# Create your tests here.
class ProductDetailTest(TestCase):
    def test_product_url_is_exist(self):
        items = self.client.post('/save-item/', data={
            'name': 'baju astrid',
            'price': 0.000,
            'category': "Dress",
            'description': "Astrid udah halu",
        })
        obj = Category.objects.first()
        response = Client().get('/product-detail/{}'.format(obj.pk))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'detailBarang/detail.html')

    def test_apakah_harga_sesuai(self):
        items = self.client.post('/save-item/', data={
            'name': 'baju astrid',
            'price': 0.000,
            'category': "Dress",
            'description': "Astrid udah halu",
        })
        field_name = 'price'
        obj = Category.objects.first()
        field_value = getattr(obj, field_name)
        self.assertEqual(0.000, field_value)

    def test_apakah_deskripsi_sesuai(self):
        items = self.client.post('/save-item/', data={
            'name': 'baju astrid',
            'price': 0.000,
            'category': "Dress",
            'description': "Astrid udah halu",
        })
        field_name = 'description'
        obj = Category.objects.first()
        field_value = getattr(obj, field_name)
        self.assertEqual("Astrid udah halu", field_value)

    def test_apakah_bisa_rent_item(self):
        items = self.client.post('/save-item/', data={
            'name': 'baju astrid',
            'price': 0.000,
            'category': "Dress",
            'description': "Astrid udah halu",
        })
        rent = self.client.post('/product-detail/{}'.format(Category.objects.first().pk), data={
            'name': 'baju astrid',
            'price': 0.000,
        })
        name_obj = Productnya.objects.filter(name="baju astrid")
        self.assertTrue(name_obj.exists())