from django.shortcuts import render,redirect 
from .models import Message as pesan
from .forms import MessageForm, UserForm
from itemList.models import Cart


def approval_list(request):
    if request.method == "POST":
        form = MessageForm(request.POST)
        if (form.is_valid()):
            msg = pesan()
            msg.message = form.cleaned_data['message_for_user']
            msg.save()
        form = MessageForm()
        user = UserForm()
        products = Cart.objects.all()
        context = {
            'formulir' : form,
            'products' : products,
            'user': user
        }
        return render(request, "approvalList.html", context)
    else:
        form = MessageForm()
        user = UserForm()
        products = Cart.objects.all()
        context = {
            'formulir' : form,
            'products' : products,
            'user': user
        }
        return render(request, "approvalList.html", context)

def approve(request,pk):
    if request.method == "POST":
        form = MessageForm(request.POST)
        if (form.is_valid()):
            msg = pesan()
            msg.message = form.cleaned_data['message_for_user']
            msg.save()
        form = MessageForm()
        user = UserForm()
        products = Cart.objects.all()
        context = {
            'formulir' : form,
            'products' : products,
            'user': user
        }
        return render(request, "approvalList.html", context)
    else:
        barang = Cart.objects.get(pk=pk)
        barang.status = "Approved" 
        form = MessageForm()
        user = UserForm()
        products = Cart.objects.all()
        context = {
            'formulir' : form,
            'products' : products,
            'user': user
        }
        return render(request, "approvalList.html", context)

def reject(request,pk):
    if request.method == "POST":
        form = MessageForm(request.POST)
        if (form.is_valid()):
            msg = pesan()
            msg.message = form.cleaned_data['message_for_user']
            msg.save()
        form = MessageForm()
        user = UserForm()
        products = Cart.objects.all()
        context = {
            'formulir' : form,
            'products' : products,
            'user': user
        }
        return render(request, "approvalList.html", context)
    else:
        barang = Cart.objects.get(pk=pk)
        barang.status = "Rejected" 
        form = MessageForm()
        user = UserForm()
        products = Cart.objects.all()
        context = {
            'formulir' : form,
            'products' : products,
            'user': user
        }
        return render(request, "approvalList.html", context)


