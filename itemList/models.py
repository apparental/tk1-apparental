from django.db import models
from detailBarang.models import Productnya
from approvalList.models import Message
from decimal import Decimal

# Create your models here.
class Cart(models.Model):
    cart_name = models.TextField(default="Your cart")
    name = models.TextField(default="Your name")
    price = models.DecimalField(max_digits=9, decimal_places=3, default=Decimal('0.000'))
    status = models.TextField(default="Waiting for Approval")


