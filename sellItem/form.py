from django import forms
from .models import Category
from decimal import Decimal

class addItem(forms.Form):
    attr = {
        'class' : 'form-control'
    }

    name = forms.CharField(label='Item Name', required=True, widget=forms.TextInput(attrs=attr))
    price = forms.DecimalField(max_digits=9, decimal_places=3)

    category = forms.ChoiceField(choices=Category.CATEGORY_CHOICES)
    
    description = forms.CharField(label='Item Description', required=True, widget=forms.TextInput(attrs=attr))
   


