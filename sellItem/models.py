from django.db import models

# Create your models here.
from decimal import Decimal

class Category(models.Model):
    name = models.TextField()
    price = models.DecimalField(max_digits=9, decimal_places=3, default=Decimal('0.0000'))

    DRESS = 'Dress'
    T_SHIRT = 'T-Shirt'
    SKIRT = 'Skirt'
    JUMPSUIT = 'Jumpsuit'
    PANTS_LEGGINGS = 'Pants & Leggings'
    JEANS = 'Jeans'
    SWIMWEAR = 'Swimwear'
    KNITWEAR_CARDIGANS = 'Knitwear & Cardigans'
    HOODIES_SWEATSHIRTS = 'Hoodies & Sweatshirts'
    BLAZER = 'Blazer'
    JACKET_COATS = 'Jacket & Coats'
    SLEEPWEAR = 'Sleepwear'
    ACCESSORIES = 'Accessories'
    ALL = 'All'

    CATEGORY_CHOICES = [
    (ALL, 'All'), (DRESS, 'Dress'), (T_SHIRT, 'T-Shirt'),
    (SKIRT, 'Skirt'), (JUMPSUIT, 'Jumpsuit'),
    (PANTS_LEGGINGS, 'Pants & Leggings'),
    (JEANS, 'Jeans'), (SWIMWEAR, 'Swimwear'),
    (KNITWEAR_CARDIGANS, 'Knitwear & Cardigans'),
    (HOODIES_SWEATSHIRTS, 'Hoodies & Sweatshirts'),
    (BLAZER, 'Blazer'), (JACKET_COATS, 'Jacket & Coats'),
    (SLEEPWEAR, 'Sleepwear'), (ACCESSORIES, 'Accessories')
    ]

    category = models.CharField(max_length = 100,
        choices=CATEGORY_CHOICES, default = ALL,
        )

    description = models.TextField()