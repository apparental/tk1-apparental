from django import forms

class MessageForm(forms.Form):
    message_for_user = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter a message',
        'type' : 'text',
        'required': True,
    }))

class UserForm(forms.Form):
    PERSONAL = 'personal'
    SOCIAL = 'social'
    ACADEMIC = 'academic'
    ORGANIZATIONAL = 'organizational'
    WORK = "work"
    CATEGORY_CHOICES = [
    ]
    Find_by_user = forms.ChoiceField(choices=CATEGORY_CHOICES)
