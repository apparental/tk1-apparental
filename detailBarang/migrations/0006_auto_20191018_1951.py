# Generated by Django 2.0.7 on 2019-10-18 19:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('detailBarang', '0005_productnya'),
    ]

    operations = [
        migrations.RenameField(
            model_name='productnya',
            old_name='namaproduk',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='productnya',
            old_name='hargaproduk',
            new_name='price',
        ),
    ]
