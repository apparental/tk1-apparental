from django.db import models
from decimal import Decimal
import datetime

# Create your models here.

class ProductDetail(models.Model):
    date = models.DateField(True, default=datetime.date.today)
    comment = models.CharField(max_length = 100, blank=False)

class Productnya(models.Model):
    name = models.TextField()
    price = models.DecimalField(max_digits=9, decimal_places=3, default=Decimal('0.0000'))