from django import forms
from .models import ProductDetail

class ProductDetailForm(forms.Form):
    Write_a_Comment = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Give us your feedback!',
        'type' : 'text',
        'required' : True
    }))