from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import products
from sellItem.models import Category
from .forms import CategoryForm

# Create your tests here.
class ProductsTest(TestCase):
    def test_product_url_is_exist(self):
        response = Client().get('/products/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'products/products.html')

    def test_product_url_func(self):
        found = resolve('/products/')
        self.assertEqual(found.func, products)

    def test_category_post_success_and_render_the_result(self):
        test = 'Dress'
        response_post = Client().post('/products/', {'Filter_by_category': test})
        self.assertEqual(response_post.status_code, 200)

    def test_form_validation_for_blank_items(self):
        form = CategoryForm(data = {'Filter_by_category': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Filter_by_category'], ["This field is required."])

    def test_apakah_nama_objek_ada(self):
        items = self.client.post('/save-item/', data={
            'name': 'baju astrid',
            'price': 0.000,
            'category': "Dress",
            'description': "Astrid udah halu"
        })
        field_name = 'name'
        obj = Category.objects.first()
        field_value = getattr(obj, field_name)
        self.assertEqual('baju astrid', field_value)

    def test_apakah_harga_sesuai(self):
        items = self.client.post('/save-item/', data={
            'name': 'baju astrid',
            'price': 0.000,
            'category': "Dress",
            'description': "Astrid udah halu"
        })
        field_name = 'price'
        obj = Category.objects.first()
        field_value = getattr(obj, field_name)
        self.assertEqual(0.000, field_value)

    def test_apakah_category_all_sesuai(self):
        response = self.client.post('/products/', data={
            'Filter_by_category': 'All',
        })
        self.assertEqual(response.status_code, 200)

    

    
