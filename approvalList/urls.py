from django.urls import path
from .views import approval_list
from .views import approve
from .views import reject


appname = 'approvalList'

urlpatterns = [
   path('', approval_list, name = 'approvalList'),
   path('', approve, name = 'approval'),
   path('', reject, name = 'rejected')
]