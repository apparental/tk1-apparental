from django.urls import path
from .views import homepage

appname = 'homepage'

urlpatterns = [
    path('',homepage,name='homepage')
]