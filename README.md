# Pipeline and Coverage

[![pipeline status](https://gitlab.com/gibranbrahmanta/tk-1-ppw/badges/master/pipeline.svg)](https://gitlab.com/gibranbrahmanta/tk-1-ppw/commits/master)
[![coverage report](https://gitlab.com/gibranbrahmanta/tk-1-ppw/badges/master/coverage.svg)](https://gitlab.com/gibranbrahmanta/tk-1-ppw/commits/master)

## Intro

- URL   : [apparental](https://apparental.herokuapp.com/)

- Group **KE05** :
	- 1806147092 - Novia Ramadani
    - 1806186553 - Gibran Brahmanta Patriajati
	- 1806186654 - Akmal Farhan Raiyan
	- 1806235826 - Astrida Nayla Fauzia

## About apparental

Apparental berguna bagi orang yang berkeinginan untuk menyewa pakaian. 

## Features

- HomePage:
	- Admin
	- Member
- Admin :
	- Create/add rented items 
	- Items list  
- Member :
	- Items list 
	- Rent items 
	- Rented items status 