from django.test import TestCase, Client
from django.urls import resolve
from .views import approval_list
from .models import Message
from .forms import MessageForm

# Create your tests here.
class approvalListUrlTests(TestCase):

    def test_start_url(self):
        response = Client().get('/approval-list/')
        self.assertEqual(response.status_code,200)

    def test_func(self):
        found = resolve('approval-list')
        self.assertEqual(found.func, approval_list)

    def test_redirect(self):
        response = Client().post('/approval-list/')
        self.assertEqual(response.status_code, 200)
    
    def test_item_saving(self):
    	test_item = Message(id=1, name='gibran', message='ui sehat mental')
        self.assertNotIsInstance(test_item,Message)
        self.assertEqual(Message.objects.count(),0)
    
    def test_if_form_is_valid(self):
        form = MessageForm(data={'message_for_user':'minimal b+ la ya ppw amin'})
        self.assertTrue(form.is_valid)
    

