from django.shortcuts import render, redirect
from .forms import ProductDetailForm
from .models import ProductDetail as Komentar
from .models import Productnya
from sellItem.models import Category
# Create your views here.

def detailBarang(request, pk):
    if request.method == 'POST':
        form = ProductDetailForm(request.POST)
        if (form.is_valid()):
            komentarBaru = Komentar()
            komentarBaru.comment = form.cleaned_data['Write_a_Comment']
            komentarBaru.save()
        form = ProductDetailForm()
        comments = Komentar.objects.all()
        items = Category.objects.filter(pk=pk)
        barang = Category.objects.get(pk=pk)
        context = {
            'formulir' : form,
            'comments' : comments,
            'items' : items
        }
        produk = Productnya()
        produk.name = barang.name
        produk.price = barang.price
        produk.save()
        return render(request, 'detailBarang/detail.html', context)
    else:
        form = ProductDetailForm()
        comments = Komentar.objects.all()
        items = Category.objects.filter(pk=pk)
        context = {
            'formulir' :  form,
            'comments' : comments,
            'items' : items
        }
        return render(request, 'detailBarang/detail.html', context)