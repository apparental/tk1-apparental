from django.shortcuts import render
from .models import Cart
from .forms import CreateCartForm
from detailBarang.models import Productnya

# Create your views here.
# def itemcart(request):
#     return render(request, 'schedule.html')

def itemcart(request):
    if request.method == "POST":
        form = CreateCartForm(request.POST)
        if (form.is_valid()):
            products = Productnya.objects.all()
            for objects in products:
                cart = Cart()
                cart.cart_name = form.cleaned_data['Cart_name']
                cart.name = object.name
                cart.price = object.price
                cart.save()
        form = CreateCartForm()
        products = Productnya.objects.all()
        context = {
            'formulir' : form,
            'products' : products
        }
        return render(request, "itemList.html", context)
    else:
        form = CreateCartForm()
        products = Productnya.objects.all()
        context = {
            'formulir' : form,
            'products' : products
        }
        return render(request, "itemList.html", context)

#     # data = Baju.objects.all()
#     # pesan = Message.objects.all()
#     form = forms.CreateCart()
#     # if request.method == "GET":
#     #     status = request.GET.get("return")
#     #     if status == "":

#     return render(request, "itemlist.html", {"data":data, "pesan":pesan, "form":form})

# def createcart(request):
#     form = forms.CreateCart()
#     return render(request, 'schedule.html', {"form":form})
